library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.all;

entity tb is
end entity tb;

architecture RTL of tb is
	
	constant CLK_PERIOD : time := 10 ns;
	constant NUM_PORTS  : positive := 3;
	
	signal clk : stD_logic                          := '0';
	signal req : std_logic_vector(0 to NUM_PORTS-1) := (others => '0');
	signal ack : std_logic_vector(0 to NUM_PORTS-1) := (others => '0');
	
begin

	clk <= not clk after CLK_PERIOD / 2.0;
	
	sim_proc : process
	begin
	
		wait until rising_edge(clk);
		
		for stim in 1 to 2**NUM_PORTS-1 loop
			report "Stim: " & integer'image(stim);
			req <= std_logic_vector(to_unsigned(stim, req'length));
			wait until rising_edge(clk);
			while (req /= (req'range => '0')) loop
				wait until ack'event;
				for i in ack'range loop
					if (ack(i) = '1') then
						report "Reset " & integer'image(i);
						req(i) <= '0';
					end if;
				end loop;
			end loop;
			wait until rising_edge(clk);
		end loop;
		
		for stim in 2**NUM_PORTS-1 downto 1 loop
			report "Stim: " & integer'image(stim);
			req <= std_logic_vector(to_unsigned(stim, req'length));
			wait until rising_edge(clk);
			while (req /= (req'range => '0')) loop
				wait until ack'event;
				for i in ack'range loop
					if (ack(i) = '1') then
						report "Reset " & integer'image(i);
						req(i) <= '0';
					end if;
				end loop;
			end loop;
			wait until rising_edge(clk);
		end loop;
		
		stop;
	
	end process;

	uut : entity work.arbiter
		generic map(
			N => NUM_PORTS
		)
		port map(
			clk_in => clk,
			req_in => req,
			ack_q  => ack
		);

end architecture RTL;
