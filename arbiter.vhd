library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity arbiter is
	generic(
		N      : positive 
	);
	port(
		clk_in : in  std_logic;
		req_in : in  std_logic_vector(0 to N-1);
		ack_q  : out std_logic_Vector(0 to N-1)
	);
end entity arbiter;

architecture RTL of arbiter is

	type t_matrix is array(0 to N-1) of std_logic_vector(0 to N-1);
	
	function init_matrix return t_matrix is
		variable init : t_matrix := (others => (others => '1'));
	begin
		for i in 1 to N-1 loop
			for j in 0 to i-1 loop
				init(i)(j) := '0';
			end loop;
		end loop;
		return init;
	end function;
	
	signal matrix : t_matrix := init_matrix;

	signal ack    : std_logic_vector(ack_q'range) := (others => '0');

begin

	assert N > 1 report "N must be larger 1!" severity failure;

	ack_q <= ack;

	gen_arbiters : for i in 0 to N-1 generate
	begin
	
		det_proc : process (clk_in)
		begin
		
			if (rising_edge(clk_in)) then
				if (req_in(i) = '0') then
					ack(i) <= '0';
				else				
					ack(i) <= '1';
					for j in 0 to N-1 loop
						if (req_in(j) = '1' and matrix(i)(j) = '0') then
							ack(i) <= '0';
						end if;
					end loop;				
				end if;
			end if;		
		
		end process;
	
	end generate;
	
	matrix_proc : process (ack)
	begin

		for i in 0 to N-1 loop
			if (ack(i) = '1') then
				-- set Rows '0'
				matrix(i) <= (others => '0');
				-- set Columns '1'
				for j in 0 to N-1 loop
					matrix(j)(i) <= '1';
				end loop;
			end if;
		end loop;
	
	end process;

end architecture RTL;
